function load() {
  hideResults();
}
function hideResults() {
  var resultsDiv = document.getElementById("resultsDiv");
  resultsDiv.style.display = "none"
}
function startGame() {
  var money = parseInt(document.getElementById("startingMoney").value);
  if (isNaN(money)) {
    window.alert("Please enter a starting bet");
    return;
  }
  var rolls = 0;
  var amounts = [];
  amounts.push(money);
  while (money > 0) {
    diceSum = rollDice();
    rolls ++;
    if (diceSum == 7) {
      money += 4;
    } else {
      money --;
    }
    amounts.push(money);
  }
  showResults(amounts, rolls);
}
function rollDice() {
  return Math.floor(Math.random() * (12 - 2 + 1)) + 2;
}
function checkHighest(amounts) {
  var highest = Math.max.apply(Math, amounts);
  var highestRolls = amounts.indexOf(highest);

  return [highest, highestRolls];
}
function showResults(amounts, rolls) {
  var resultsDiv = document.getElementById("resultsDiv");
  resultsDiv.style.display = "inline-block";
  var results = checkHighest(amounts);
  document.getElementById("startingBet").innerHTML = "$" + amounts[0];
  document.getElementById("totalRolls").innerHTML = rolls;
  document.getElementById("highestAmountWon").innerHTML = "$" + results[0];
  document.getElementById("rollCount").innerHTML = results[1];
  document.getElementById("startButton").innerHTML = "Play Again";
  document.getElementById("startingMoney").value = "";
}
