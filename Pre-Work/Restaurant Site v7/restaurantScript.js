function check() {
  checkContactInfo();
  checkReasons();
  checkDays();
}

function checkContactInfo() {
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var phone = document.getElementById("phone").value;
  if (name == "") {
    contactInfoWarning();
  } else if (email == "" && phone == "") {
    contactInfoWarning();
  }
}

function contactInfoWarning() {
  window.alert("Please enter your name and either an email address or phone number");
}

function checkReasons() {
  var reasonForInquiry = document.getElementById("reasonForInquiry").value;
  var additionalInformation = document.getElementById("additionalInformation").value;
  if (reasonForInquiry == "other" && additionalInformation.length < 1) {
    reasonsWarning();
  }
}

function reasonsWarning() {
  window.alert("Please provide a reason for your inquiry in the 'Additional Information' box")
}

function checkDays() {
  var days = document.getElementsByName("days");
  var checked = 0;
  for (i = 0; i < days.length; i++) {
    if (days[i].checked) {
      checked = 1;
    }
  }
  if (checked == 0) {
    daysWarning();
  }
}

function daysWarning() {
  window.alert("Please let us know the best days to contact you")
}
